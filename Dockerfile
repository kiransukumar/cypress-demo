# This Docker file is for building this project on Codeship Pro
# https://documentation.codeship.com/pro/languages-frameworks/nodejs/

# use Cypress provided image with all dependencies included
FROM cypress/base:16.13.0

#ENTRYPOINT ["npx", "cypress", "run"]

RUN node --version
RUN npm --version
WORKDIR /home/node/app
COPY package.json package-lock.json cypress.json ./
# copy our test application
COPY cypress ./cypress


# avoid many lines of progress bars during install
# https://github.com/cypress-io/cypress/issues/1243
ENV CI=1

# install NPM dependencies and Cypress binary
RUN npm install
# check if the binary was installed successfully
RUN $(npm bin)/cypress verify
