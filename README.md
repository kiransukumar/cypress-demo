Initialize npm project
npm init

Install cypress
npm install cypress@9.7.0 --save-dev

Open cypress for the first time and initialize a cypress project creation
npx cypress open

To build a docker image based on the docker file
docker build -t cypress-demo .

To check if the image is created
docker images

To run the docker image
docker run -d 
